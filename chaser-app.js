var RSA_PRIVATE_KEY;
Invoices = new Mongo.Collection("invoices");

if(Meteor.isClient){
    Template.body.events({
        'click button': function () {
            // Only update invoices when asked to do so
            Meteor.call('get_invoices');
        }
    });
    Template.body.helpers({
        invoices: function(){
            // Display invoices ordered by descending DueDate
            return Invoices.find({}, {sort: {DueDate:-1}});
        }
    });
    Template.invoice.helpers({
        format_date: function(context, options){
            // Format invoice dates in a nicer-to-read fashion
            return moment(context).format('DD/MM/YYYY');       
        }
    });
}

if(Meteor.isServer){
    Meteor.methods({
        'get_invoices': function get_invoices(){
            var Xero = Meteor.npmRequire('xero'); // Needed to use a slightly older version of the package (0.0.6)

            // Load key and secret from settings
            var CONSUMER_KEY = Meteor.settings.xero_consumer_key;
            var CONSUMER_SECRET = Meteor.settings.xero_consumer_secret;

            // If private key not yet loaded, then load it from file
            if(RSA_PRIVATE_KEY == null){
                RSA_PRIVATE_KEY = Assets.getText("privatekey.pem");
            }

            // Make request to Xero's API
            new Xero(CONSUMER_KEY, CONSUMER_SECRET, RSA_PRIVATE_KEY).call('GET', '/Invoices', null, Meteor.bindEnvironment(function(err, res) {
                if(!err){
                    _.each(res.Response.Invoices.Invoice, function(invoice){
                        // Store each invoice
                        invoice._id = invoice.InvoiceID;
                        try{
                            Invoices.update({_id:invoice._id}, invoice, {upsert:true}); // Update if exists, create if not
                        }
                        catch(update_error){
                            // Handle error, if necessary
                        }
                    });
                }
                else{
                    // Handle error, if necessary
                }
            }));
        }
    });
}
